/***
 Incluir:
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
 * */

$(document).ready(function () {
    $('#celular').mask('(00) 0000-00000');
    $('#telefone').mask('(00) 0000-00000');
    $('#cpf').mask('000.000.000-00');
});